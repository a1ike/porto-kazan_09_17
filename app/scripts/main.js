$(document).ready(function () {

  $('.pk-drop__droper').click(function () {
    $('.pk-drop__nav').slideToggle('fast', function () {
      // Animation complete.
    });
  });

  $('.pk-reviews').slick({
    dots: false,
    arrows: true,
    infinite: true,
    autoplay: true,
    speed: 300,
    slidesToShow: 1
  });

});